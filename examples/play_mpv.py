import subprocess
import sys

from libssp_py.ssp import PacketType, SSPClient


if len(sys.argv) > 1:
	# If an IP address is provided, connect to that IP
	host = sys.argv[1]
else:
	# Otherwise, try using the discovery protocol
	from libssp_py.discovery import Discovery

	print("Discovering SSP devices...")
	print()
	i = 0
	ls = []
	def callback(hostname):
		global i
		print(f" {i}: {hostname}")
		ls.append(hostname)
		i += 1
	
	l = Discovery(callback)
	i = int(input())
	l.stop()
	host = ls[i].strip('.')

print('Connecting to', host, '...')

# Connect to the SSP device

ssp = SSPClient(host)
ssp.connect()

print('Connected!')

# Start two instances of mpv, one for video and one for audio

mpv = subprocess.Popen(['mpv', '-', '--quiet', '--no-correct-pts'], stdin=subprocess.PIPE)
mpv2 = subprocess.Popen(['mpv', '-', '--quiet', '--no-correct-pts'], stdin=subprocess.PIPE)


def process_video(data: bytes):
	# header = data[:36]
	mpv.stdin.write(data[36:])

def process_audio(data: bytes):
	# header = data[:28]
	mpv2.stdin.write(data[28:])


for data in ssp.stream_packets():
	try:
		packet_type = PacketType(data[0])

		if packet_type == PacketType.STREAM_VIDEO:
			process_video(data[1:])
		elif packet_type == PacketType.STREAM_AUDIO:
			process_audio(data[1:])

	except (ValueError, KeyError):
		pass